# Translation of Odoo Server.
# This file contains the translation of the following modules:
#	* fso_base_website
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 8.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-02-06 12:59+0000\n"
"PO-Revision-Date: 2018-02-06 12:59+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: fso_base_website
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_web_survey_user_input
msgid "Answers"
msgstr "Antworten"

#. module: fso_base_website
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_web_crm_oppor
msgid "Bearbeitete Anfragen"
msgstr "Bearbeitete Anfragen"

#. module: fso_base_website
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_web_blog
msgid "Blog"
msgstr "Blog"

#. module: fso_base_website
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_web_blog_blogs
msgid "Blogs"
msgstr "Blogs"

#. module: fso_base_website
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_web_crm
msgid "CRM"
msgstr "CRM"

#. module: fso_base_website
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_web_doc
msgid "Documentation"
msgstr "Dokumentation"

#. module: fso_base_website
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_web_website_domains
#: model:ir.ui.menu,name:fso_base_website.website_domains_menu
msgid "Domain Manager"
msgstr "Domain Manager"

#. module: fso_base_website
#: model:ir.ui.menu,name:fso_base_website.domain_templates_menu
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_web_domain_templates
msgid "Domain Templates"
msgstr "Domain Templates"

#. module: fso_base_website
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_web_forum
msgid "Forum"
msgstr "Hilfeforum"

#. module: fso_base_website
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_web_forum_forums
msgid "Forums"
msgstr "Forenverzeichnis"

#. module: fso_base_website
#: view:website:fso_base_website.website_form
msgid "Google Analytics"
msgstr "Google Analytics"

#. module: fso_base_website
#: field:website,google_analytics_script:0
msgid "Google Analytics Script"
msgstr "Google Analytics Script"

#. module: fso_base_website
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_web_survey_label_form
msgid "Labels"
msgstr "Überschriften"

#. module: fso_base_website
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_web_landing_pages_manager
#: model:ir.ui.menu,name:fso_base_website.widget_manager_lp_menu
msgid "LandingPage Manager"
msgstr "LandingPage Manager"

#. module: fso_base_website
#: view:website:fso_base_website.website_form
msgid "Name"
msgstr "Bezeichnung"

#. module: fso_base_website
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_web_crm_leads
msgid "Neue Anfragen"
msgstr "Neue Anfragen"

#. module: fso_base_website
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_web_survey_page_form
msgid "Pages"
msgstr "Seiten"

#. module: fso_base_website
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_web_blog_posts
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_web_doc_posts
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_web_forum_posts
msgid "Posts"
msgstr "Beiträge"

#. module: fso_base_website
#: field:website,PublicPartnerNoSubscribe:0
msgid "Public Partner: Do not add as follower"
msgstr "Öffentlichen Benutzer nicht autom. als Follower hinzufügen"

#. module: fso_base_website
#: field:website,PublicPartnerTimezone:0
msgid "Public Partner: Timezone"
msgstr "Zeitzone des Öffentlichen Benutzers"

#. module: fso_base_website
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_web_survey_question_form
msgid "Questions"
msgstr "Fragestellungen"

#. module: fso_base_website
#: view:website:fso_base_website.website_form
msgid "Search Engines"
msgstr "Suchmaschinen"

#. module: fso_base_website
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_web_settings
msgid "Settings"
msgstr "Einstellungen"

#. module: fso_base_website
#: view:website:fso_base_website.website_form
msgid "Social Media Accounts"
msgstr "Social Media Konten"

#. module: fso_base_website
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_web_survey
msgid "Survey"
msgstr "Umfrage"

#. module: fso_base_website
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_web_survey_user_input_line
msgid "Survey User Input lines"
msgstr "Teilnehmer Eingabezeilen"

#. module: fso_base_website
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_web_survey_surveys
msgid "Surveys"
msgstr "Umfragen"

#. module: fso_base_website
#: view:website:fso_base_website.website_form
msgid "TIP: If no default timezone is set the standard timzone of the selected country will\n"
"                                    be used if possible. Make sure the timezone is also synced for partners synced\n"
"                                    from Fundraising Studio."
msgstr "TIP: Wenn keine Zeitzone gesetzt ist wird, wann immer möglich, die standard Zeitzone des Landes verwendet.\n"
"                                    Bitte stellen Sie sicher, daß die Zeitzone von Kontakten ebenfalls mit Fundraising Studio ausgetauscht\n"
"                                    wird."

#. module: fso_base_website
#: view:website:fso_base_website.website_form
msgid "TIP: This prevents that the donor is added as a follower automatically\n"
"                                    and therefore most (possibly unwanted) E-Mails from FS-Online related to document\n"
"                                    status changes."
msgstr "TIP: Diese Einstellung verhindert, dass der Kontakt (Benutzer/Spender) automatisch als Follower bei durch ihn erstellten Datensätzen hinzugefügt wird\n"
"                                    Dies bedeutet das er bei Statusänderungen der von Ihm erzeugten Dokumente (Aufträge, Zahlungen, ...)\n"
"                                     keine generischen Informations E-Mails erhällt. Wesentliche E-Mails wie das Danke E-Mail werden jedoch dennoch zugestellt."

#. module: fso_base_website
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_web_doc_toc
msgid "Table of Content"
msgstr "Inhaltsverzeichnis"

#. module: fso_base_website
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_web_blog_tags
msgid "Tags"
msgstr "Schlagwörter"

#. module: fso_base_website
#: view:website:fso_base_website.website_form
msgid "The default is Enabled."
msgstr "Im Standard aktiviert."

#. module: fso_base_website
#: help:website,PublicPartnerTimezone:0
msgid "The partner's timezone, used to output proper date and time values inside printed reports. It is important to set a value for this field. You should use the same timezone that is otherwise used to pick and render date and time values: your computer's timezone."
msgstr "Die Zeitzone des Kontaktes bestimmt das Datums und Zeitformat für das Backend, die Webseite sowie für PDF-Reports. Daher ist es sehr wichtig die Zeitzone für jeden Kontakt korrekt zu setzen."

#. module: fso_base_website
#: view:website:website.robots
msgid "User-agent: *\n"
"                    Disallow: /certifications\n"
"                    Disallow: /payment\n"
"                    Disallow: /website_popup\n"
"                    Disallow: /shop/confirmation_static\n"
"                    Disallow: /shop/ogonepayment.html\n"
"                    Disallow: /shop/confirm_order\n"
"                    Disallow: /shop/confirmation\n"
"                    Disallow: /shop/ogonepayment\n"
"                    Disallow: /shop/checkout\n"
"                    Disallow: /shop/payment\n"
"                    Disallow: /shop/cart\n"
"                    Disallow: /crm/contactus\n"
"                    Disallow: /web/register\n"
"                    Disallow: /web/reset_password\n"
"                    Disallow: /web/login\n"
"                    Disallow: /event/get_country_event_list\n"
"                    Disallow: /forum/get_tags\n"
"                    Disallow: /aswidget\n"
"                    Disallow: /as_widget\n"
"                    Disallow: /members\n"
"                    Disallow: /groups\n"
"                    Disallow: /register\n"
"                    Disallow: /partners/country\n"
"                    Disallow: /jobs/country\n"
"                    Disallow: /certifications\n"
"\n"
"                    # Multilanguage:\n"
"                    Disallow: /*/payment\n"
"                    Disallow: /*/website_popup\n"
"                    Disallow: /*/shop/confirmation_static\n"
"                    Disallow: /*/shop/ogonepayment.html\n"
"                    Disallow: /*/shop/confirm_order\n"
"                    Disallow: /*/shop/confirmation\n"
"                    Disallow: /*/shop/ogonepayment\n"
"                    Disallow: /*/shop/checkout\n"
"                    Disallow: /*/shop/payment\n"
"                    Disallow: /*/shop/cart\n"
"                    Disallow: /*/crm/contactus\n"
"                    Disallow: /*/web/register\n"
"                    Disallow: /*/web/reset_password\n"
"                    Disallow: /*/web/login\n"
"                    Disallow: /*/event/get_country_event_list\n"
"                    Disallow: /*/forum/get_tags\n"
"                    Disallow: /*/aswidget\n"
"                    Disallow: /*/as_widget\n"
"                    Disallow: /*/members\n"
"                    Disallow: /*/groups\n"
"                    Disallow: /*/register\n"
"                    Disallow: /*/partners/country\n"
"                    Disallow: /*/jobs/country\n"
"                    Sitemap:"
msgstr "User-agent: *\n"
"                    Disallow: /certifications\n"
"                    Disallow: /payment\n"
"                    Disallow: /website_popup\n"
"                    Disallow: /shop/confirmation_static\n"
"                    Disallow: /shop/ogonepayment.html\n"
"                    Disallow: /shop/confirm_order\n"
"                    Disallow: /shop/confirmation\n"
"                    Disallow: /shop/ogonepayment\n"
"                    Disallow: /shop/checkout\n"
"                    Disallow: /shop/payment\n"
"                    Disallow: /shop/cart\n"
"                    Disallow: /crm/contactus\n"
"                    Disallow: /web/register\n"
"                    Disallow: /web/reset_password\n"
"                    Disallow: /web/login\n"
"                    Disallow: /event/get_country_event_list\n"
"                    Disallow: /forum/get_tags\n"
"                    Disallow: /aswidget\n"
"                    Disallow: /as_widget\n"
"                    Disallow: /members\n"
"                    Disallow: /groups\n"
"                    Disallow: /register\n"
"                    Disallow: /partners/country\n"
"                    Disallow: /jobs/country\n"
"                    Disallow: /certifications\n"
"\n"
"                    # Multilanguage:\n"
"                    Disallow: /*/payment\n"
"                    Disallow: /*/website_popup\n"
"                    Disallow: /*/shop/confirmation_static\n"
"                    Disallow: /*/shop/ogonepayment.html\n"
"                    Disallow: /*/shop/confirm_order\n"
"                    Disallow: /*/shop/confirmation\n"
"                    Disallow: /*/shop/ogonepayment\n"
"                    Disallow: /*/shop/checkout\n"
"                    Disallow: /*/shop/payment\n"
"                    Disallow: /*/shop/cart\n"
"                    Disallow: /*/crm/contactus\n"
"                    Disallow: /*/web/register\n"
"                    Disallow: /*/web/reset_password\n"
"                    Disallow: /*/web/login\n"
"                    Disallow: /*/event/get_country_event_list\n"
"                    Disallow: /*/forum/get_tags\n"
"                    Disallow: /*/aswidget\n"
"                    Disallow: /*/as_widget\n"
"                    Disallow: /*/members\n"
"                    Disallow: /*/groups\n"
"                    Disallow: /*/register\n"
"                    Disallow: /*/partners/country\n"
"                    Disallow: /*/jobs/country\n"
"                    Sitemap:"

#. module: fso_base_website
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_web
msgid "Webseite"
msgstr "Webseite"

#. module: fso_base_website
#: model:ir.actions.act_window,name:fso_base_website.website_action
#: model:ir.model,name:fso_base_website.model_website
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_web_website_menu
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_website
#: model:ir.ui.menu,name:fso_base_website.website_menu
#: view:website:fso_base_website.website_form
msgid "Website"
msgstr "Webseite"

#. module: fso_base_website
#: view:website:fso_base_website.website_form
msgid "Website Public Partner and User"
msgstr "Öffentlicher Webseiten-Kontakt und Benutzer"

#. module: fso_base_website
#: view:website:fso_base_website.website_form
msgid "Website Settings"
msgstr "Webseite"

#. module: fso_base_website
#: view:website:fso_base_website.website_search
#: view:website:fso_base_website.website_tree
msgid "Websites"
msgstr "Webseiten"

#. module: fso_base_website
#: model:ir.ui.menu,name:fso_base_website.menu_fsonline_web_widget_manager
#: model:ir.ui.menu,name:fso_base_website.widget_manager_menu
msgid "Widget Manager"
msgstr "Widget Manager"

#. module: fso_base_website
#: view:website:fso_base_website.website_form
#: field:website,RobotsArch:0
msgid "robots.txt"
msgstr "robots.txt"

#. module: fso_base_website
#: field:website,RobotsViewId:0
msgid "robots.txt id"
msgstr "robots.txt id"

#. module: fso_base_website
#: view:website:website.robots
msgid "sitemap.xml"
msgstr "sitemap.xml"

